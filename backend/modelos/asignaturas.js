const mongoose = require('mongoose');
const { Schema } = mongoose;
const { appConfig } = require('../config')

const AsignaturaSchema = new Schema({
    id: { type: String, required: true },
    nombre: { type: String, required: true },
    anio: { type: String, required: true },
    descripcion: { type: String, required: false },
    pdfUrl:{ type : Array , "default" : [] }
});


/*AsignaturaSchema.method.setpdfUrl = function setpdfUrl(filename){ // se el metodo method de mongoose para agregar un metodo a el esquema 
                                                    // este metodo se encargara de agregar la url del archivo a la asignatura
 const {host, port} = appConfig // estas configuraciones de host y port se encuentran en el archivo config de la ruta priencipal (backend)
 this.pdfUrl = `${host}:${port}/public/${filename}` // este sera el nombre de la url que se almacena , el /public sera mapeado
                                                    // internamente a backend/storage/pdfs para que el cliente no sepa la direccion
                                                    // interna en donde se
}*/



module.exports = mongoose.model('Asignatura', AsignaturaSchema);