const mongoose = require('mongoose');
const { Schema } = mongoose;
var bcrypt = require('bcrypt');


const DocenteSchema = new Schema({
    id: { type: String, required: true },
    email: { type: String, required: true },
    password: { type: String, required: true },
    nombre: { type: String, required: true },
    apellido: { type: String, required: true },
    asignatura: [{ type: Schema.ObjectId, ref: "Asignatura" }]
});

DocenteSchema.statics.hashPassword = function hashPassword(password) {
    return bcrypt.hashSync(password, 10);
}

DocenteSchema.methods.isValid = function(hashedpassword) {
    return bcrypt.compareSync(hashedpassword, this.password);
}

module.exports = mongoose.model('Docente', DocenteSchema);