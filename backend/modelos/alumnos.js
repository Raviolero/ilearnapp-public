const mongoose = require('mongoose');
const { Schema } = mongoose;

const AlumnoSchema = new Schema({
    id: { type: Number, required: true },
    nombreUsuario: { type: String, required: true },
    password: { type: String, required: true },
    nombre: { type: String, required: true },
    apellido: { type: String, required: true },
});


module.exports = mongoose.model('Alumno', AlumnoSchema);