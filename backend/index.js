const express = require('express');
const morgan = require('morgan');
const app = express();
const { mongoose } = require('./database');
const cors = require('cors');

//Configuraciones
app.set('puerto', process.env.PORT || 3000);

//Middleware
app.use(morgan('dev'));
app.use(express.json());

app.use(cors({
    origin: ['http://localhost:4200', 'http://127.0.0.1:4200'],
    credentials: true
}));

// passport

var passport = require('passport');
var session = require('express-session');
app.use(session({
    name: 'muname.sid',
    resave: false,
    saveUninitialized: false,
    secret: 'secret',
    cookie: {
        maxAge: 36000000,
        httpOnly: false,
        secure: false
    }
}));
require('./passport-config');
app.use(passport.initialize());
app.use(passport.session());

//Rutas
app.use('/api/alumnos', require('./rutas/alumnos.rutas'));
app.use('/api/login', require('./rutas/login'));
app.use('/api/admin', require('./rutas/admin'));
app.use('/api/asignaturas', require('./rutas/asignaturas.rutas'));
app.use('/api/docentes', require('./rutas/docentes.rutas'));
app.use('/public', express.static(`${__dirname}/storage/pdfs`));
//Inicio del servidor
app.listen(app.get('puerto'), () => {
    console.log('Servidor corriendo en el puerto', app.get('puerto'));
});