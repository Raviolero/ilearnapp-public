const Asignatura = require('../modelos/asignaturas');
const controladorAsignatura = {};



controladorAsignatura.getAsignaturas = async (req, res) => {
    const asignaturas = await Asignatura.find();

    res.json(asignaturas);
}

controladorAsignatura.getAsignatura = async (req, res) => {
    const asignatura = await Asignatura.findById(req.params.id);

    res.json(asignatura);
}

controladorAsignatura.createAsignatura = async (req, res) => {


    const asignatura = Asignatura({
        id: req.body.id,
        nombre: req.body.nombre,
        anio: req.body.anio,
        descripcion: req.body.descripcion,
        pdfUrl:[]
    });
    /*if(req.file){
       
        asignatura.setpdfUrl(filename);
    }*/
    const resultado = await asignatura.save();
    res.status(201).send({ resultado })

}
controladorAsignatura.nuevoArchivo = async (req, res) => {
    const { filename } = req.file;
    const asignatura = await Asignatura.update(
        { _id: req.params.id }, 
        { $push: { pdfUrl:`${'http://localhost'}:${'3000'}/public/${filename}`  } },
    );

    res.status(201).send({ asignatura })
}

controladorAsignatura.editAsignatura = async (req, res) => {
    if (req.file) {
        
        const asignatura = await Asignatura.findByIdAndUpdate(req.params.id, {
            $set: {
                id: req.body.id,
                nombre: req.body.nombre,
                anio: req.body.anio,
                descripcion: req.body.descripcion,
                $push:{"pdfUrl":`${'http://localhost'}:${'3000'}/public/${filename}`}
            }
        },{
            new: true,
            useFindAndModify: false

        });
        res.status(201).send({ asignatura })

    } else {
        const asignatura = await Asignatura.findByIdAndUpdate(req.params.id, {
            $set: {
                id: req.body.id,
                nombre: req.body.nombre,
                anio: req.body.anio,
                descripcion: req.body.descripcion
            }
        }, {
            new: true,
            useFindAndModify: false
        });
        res.status(201).send({ asignatura })
    }

}

controladorAsignatura.deleteAsignatura = async (req, res) => {
    await Asignatura.findByIdAndRemove(req.params.id);

    res.json({
        status: 'Asignatura eliminada'
    });
}

module.exports = controladorAsignatura;