const Alumno = require('../modelos/alumnos');

const controladorAlumno = {};



controladorAlumno.getAlumnos = async(req, res) => {
    const alumnos = await Alumno.find();

    res.json(alumnos);
}

controladorAlumno.getAlumno = async(req, res) => {
    const alumno = await Alumno.findById(req.params.id);

    res.json(alumno);
}

controladorAlumno.createAlumno = async(req, res) => {
    const alumno = new Alumno({
        id: req.body.id,
        nombreUsuario: req.body.nombreUsuario,
        password: req.body.password,
        nombre: req.body.nombre,
        apellido: req.body.nombre
    });
    await alumno.save();
    console.log(alumno);

    res.json({
        status: 'Alumno guardado'
    });
}

controladorAlumno.editAlumno = async(req, res) => {
    const alumno = await Alumno.findByIdAndUpdate(req.params.id, {
        $set: {
            id: req.body.id,
            nombreUsuario: req.body.nombreUsuario,
            password: req.body.password,
            nombre: req.body.nombre,
            apellido: req.body.nombre
        }
    }, { new: true });

    res.json({
        status: 'Alumno actualizado'
    });
}

controladorAlumno.deleteAlumno = async(req, res) => {
    await Alumno.findByIdAndRemove(req.params.id);

    res.json({
        status: 'Alumno eliminado'
    });
}

module.exports = controladorAlumno;