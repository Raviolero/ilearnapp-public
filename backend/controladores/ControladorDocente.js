const Docente = require('../modelos/docentes');

const controladorDocente = {};



controladorDocente.getDocentes = async(req, res) => {
    const docentes = await Docente.find().populate('asignatura');
    res.json(docentes);
}

controladorDocente.getDocente = async(req, res) => {
    const docente = await Docente.findById(req.params.id).populate('asignatura');

    res.json(docente);
}

controladorDocente.createDocente = async(req, res) => {
    const docente = new Docente({
        id: req.body.id,
        email: req.body.email,
        password: Docente.hashPassword(req.body.password),
        nombre: req.body.nombre,
        apellido: req.body.apellido,
        asignatura: req.body.asignatura

    });
    await docente.save();
    console.log(docente);

    res.json({
        status: 'Docente guardado'
    });
}

controladorDocente.editDocente = async(req, res) => {
    const docente = await Docente.findByIdAndUpdate(req.params.id, {
        $set: {
            id: req.body.id,
            email: req.body.email,
            password: Docente.hashPassword(req.body.password),
            nombre: req.body.nombre,
            apellido: req.body.apellido,
            asignatura: req.body.asignatura
        }
    }, { new: true });

    res.json({
        status: 'Docente actualizado'
    });
}

controladorDocente.deleteDocente = async(req, res) => {
    await Docente.findByIdAndRemove(req.params.id);

    res.json({
        status: 'Docente eliminado'
    });
}

controladorDocente.getDocenteEmail = async(req, res) => {
    const docente = await Docente.find(req.body.email).populate('asignatura');;

    res.json(docente);
}

module.exports = controladorDocente;