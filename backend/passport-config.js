var passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy;
var Admin = require('./modelos/admin');
var Docente = require('./modelos/docentes');

passport.use('adminlocal', new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password'
    },
    function(username, password, done) {
        Admin.findOne({ email: username }, function(err, admin) {
            if (err) { return done(err); }
            if (!admin) {
                return done(null, false, { message: 'Incorrect email.' });
            }
            if (!admin.isValid(password)) {
                return done(null, false, { message: 'Incorrect password.' });
            }

            return done(null, admin);
        });
    }
));
passport.use('docentelocal', new LocalStrategy({ // el primer parametro indica el tipo de validacion que se invocara
        usernameField: 'email',
        passwordField: 'password'
    },
    function(username, password, done) {
        Docente.findOne({ email: username }, function(err, docente) {
            if (err) { return done(err); }
            if (!docente) {
                return done(null, false, { message: 'email incorrecto.' });
            }
            if (!docente.isValid(password)) {
                return done(null, false, { message: 'Incorrect password.' });
            }
            return done(null, docente);
        });
    }
));


passport.serializeUser(function(admin, done) { //con esto passport almacena una sesion activa
    done(null, admin._id);
});

passport.deserializeUser(async function(id, done) { // esta funcion consulta para ver si esa almacenada la sesion

    const resultado = await Admin.find({ "_id": id }); // al tener dos tipos de usuaios para validar se ejecuta una consulta previa a una de las colecciones de mongo
    // si esta tiene algo entonces se procede a validar para ese usuario
    console.log(resultado.length);
    if (resultado.length > 0) {
        Admin.findById(id, function(err, admin) {
            done(err, admin);
        });
    } else { // de lo contrario se consulta a la otra coleccion y se procede a validar para el otro tipo de usuario
        Docente.findById(id, function(err, admin) {
            console.log(admin);
            done(err, admin);
        });
    }


});