
const multer = require('multer') // se requiere multer


const storage = multer.diskStorage({ // esta funcion es para manejar la ubicacion de en donde se guardaran los archivos
    destination: function (req, file, cb) {
      cb(null, './backend/storage/pdfs') // indicamos la carpeta pdfs
    },
    filename: function (req, file, cb) {
      //cb(null, `${file.fieldname}-${Date.now()}`)
      cb(null, file.fieldname +'-'+ file.originalname + '-' + Date.now()+'.pdf') // este es el nombre que tendra el archivo que recibimos
    }
  })
   
  var upload = multer({ storage: storage })

  module.exports=upload