const express = require('express');
const ruta = express.Router();

const controladorAlumno = require('../controladores/ControladorAlumno');

ruta.get('/', controladorAlumno.getAlumnos);
ruta.get('/:id', controladorAlumno.getAlumno);
ruta.post('/', controladorAlumno.createAlumno);
ruta.put('/:id', controladorAlumno.editAlumno);
ruta.delete('/:id', controladorAlumno.deleteAlumno);



module.exports = ruta;