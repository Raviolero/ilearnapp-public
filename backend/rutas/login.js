var express = require('express');
var router = express.Router();
var Admin = require('../modelos/admin');
var Docente = require('../modelos/docentes');
var passport = require('passport');


router.post('/', async function(req, res, next) { // esta es la ruta a la que llamaremos desde angular
    const resultado = await Admin.find({ "email": req.body.email })
    if (resultado.length > 0) {
        loginadmin(req, res, next);

    } else {
        logindocente(req, res, next);
    }
});

function loginadmin(req, res, next) {
    passport.authenticate('adminlocal', function(err, admin, info) {
        if (err) { return res.status(501).json(err); }
        if (!admin) { return res.status(403).json(info); }
        req.logIn(admin, function(err) {
            if (err) { return res.status(501).json(err); }
            return res.status(200).json({ message: 'Login Success', tipo: 'admin' });
        });
    })(req, res, next);
}

function logindocente(req, res, next) {
    passport.authenticate('docentelocal', function(err, docente, info) {
        if (err) { return res.status(501).json(err); }
        if (!docente) { return res.status(403).json(info); }
        req.logIn(docente, function(err) {
            if (err) { return res.status(501).json(err); }

            return res.status(200).json({ message: 'Login Success', tipo: 'docente' });
        });
    })(req, res, next);
}


router.get('/validar', isValidUser, function(req, res, next) {
    return res.status(200).json(req.user); // esta es la propiedad user del objeto req
});

router.get('/logout', isValidUser, function(req, res, next) {
    req.logOut();
    return res.status(200).json({ message: 'Logout Success' });

});

function isValidUser(req, res, next) {
    if (req.isAuthenticated()) next(); // este metodo es proprsionado por passport y se encarga de autenticar automaticamente a traves de cookies si el usuario se encuentra autenticado
    else return res.status(401).json({ message: 'No autorizado' }); // en caso de que no este autenticado se envian un error
}


module.exports = router;