const express = require('express');
const ruta = express.Router();
const upload = require('../libs/storage')

const controladorAsignatura = require('../controladores/ControladorAsignatura');

ruta.get('/', controladorAsignatura.getAsignaturas);
ruta.get('/:id', controladorAsignatura.getAsignatura);
ruta.post('/', controladorAsignatura.createAsignatura);
ruta.put('/:id',controladorAsignatura.editAsignatura);
ruta.delete('/:id', controladorAsignatura.deleteAsignatura);
ruta.put('/archivo/:id',upload.single('pdf'),controladorAsignatura.nuevoArchivo);



module.exports = ruta;