const express = require('express');
const ruta = express.Router();

const controladorDocente = require('../controladores/ControladorDocente');

ruta.get('/', controladorDocente.getDocentes);
ruta.get('/:id', controladorDocente.getDocente);
ruta.get('/:email', controladorDocente.getDocenteEmail);
ruta.post('/', controladorDocente.createDocente);
ruta.put('/:id', controladorDocente.editDocente);
ruta.delete('/:id', controladorDocente.deleteDocente);



module.exports = ruta;