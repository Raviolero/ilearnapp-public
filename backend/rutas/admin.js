var express = require('express');
var router = express.Router();
var Admin = require('../modelos/admin');
var passport = require('passport');

router.post('/register', function(req, res, next) { // esta es la ruta a la que llamaremos desde angular
    addToDB(req, res)
});

router.get('/', async function(req, res, next) { // esta es la ruta a la que llamaremos desde angular
    const admins = await Admin.find();
    res.json(admins);
});

async function addToDB(req, res) { // creara un usuario para mpngoDB con los datos traidos desde angular en req
    var admin = new Admin({
        email: req.body.email,
        password: Admin.hashPassword(req.body.password),
        tipo: req.body.tipo
    });
    try {
        doc = await admin.save(); // intentamos guardar  el usuario usarndo await por que puede demorar
        return res.status(201).json(doc); // si lo hace respondemos con un @OK@ y el doc en formato json
    } catch (err) {
        return res.status(501).json(err); // si no lo hace respondemos con un error
    }
}



router.post('/login', loginadmin); // llama a la funcion de abajo

function loginadmin(req, res, next) {
    passport.authenticate('adminlocal', function(err, admin, info) {
        if (err) { return res.status(501).json(err); }
        if (!admin) { return res.status(403).json(info); }
        req.logIn(admin, function(err) {
            if (err) { return res.status(501).json(err); }
            return res.status(200).json({ message: 'Login Success' });
        });
    })(req, res, next);
}



router.get('/adminHome', isValidUser, function(req, res, next) {
    return res.status(200).json(req.user); // esta es la propiedad user del objeto req
});

router.get('/logout', isValidUser, function(req, res, next) {
    req.logOut();
    return res.status(200).json({ message: 'Logout Success' });

});

function isValidUser(req, res, next) {
    if (req.isAuthenticated()) next(); // este metodo es proprsionado por passport y se encarga de autenticar automaticamente a traves de cookies si el usuario se encuentra autenticado
    else return res.status(401).json({ message: 'No autorizado' }); // en caso de que no este autenticado se envian un error
}

module.exports = router;