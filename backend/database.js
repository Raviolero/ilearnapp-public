const mongoose = require('mongoose');

const URI = 'mongodb://localhost/ilearn';

mongoose.connect(URI, { useNewUrlParser: true, useUnifiedTopology: true }) // estos dos parametros son solo para quitar los herrores de conexion que tira mongoose por modulos deprecados
    .then(db => console.log('La BD en MongoDB ha sido conectada con exito'))
    .catch(err => console.error(err));
module.exports = mongoose;


