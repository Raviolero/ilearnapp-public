import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AdminhomeComponent } from './adminhome/adminhome.component';
import { AlumnosComponent } from './componentes/alumnos/alumnos.component';
import { VincularDesvincularComponent } from './componentes/vincular-desvincular/vincular-desvincular.component';
import { DocentehomeComponent } from './componentes/docentehome/docentehome.component';
import { IslogGuard } from './guards/islog.guard';
import { IsadminGuard } from './guards/isadmin.guard';
import { HomeAsignaturaComponent } from './componentes/home-asignatura/home-asignatura.component';

const routes: Routes = [
{path:'',redirectTo:'login',pathMatch:'full'},
{path:'login',component:LoginComponent},
{path:'adminHome',component:AdminhomeComponent,canActivate:[IslogGuard,IsadminGuard]},
{path:'alumnos',component:AlumnosComponent,canActivate:[IslogGuard]},
{path:'vincular-desvincular',component:VincularDesvincularComponent,canActivate:[IslogGuard,IsadminGuard]},
{path:'docenteHome',component:DocentehomeComponent,canActivate:[IslogGuard]},
{path:'asignaturaHome/:idDocente/:idAsignatura',component:HomeAsignaturaComponent,canActivate:[IslogGuard]}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
