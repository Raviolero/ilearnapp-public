import { TestBed } from '@angular/core/testing';

import { UploadArchivoService } from '../uploadArchivo/upload-archivo.service';

describe('UploadArchivoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UploadArchivoService = TestBed.get(UploadArchivoService);
    expect(service).toBeTruthy();
  });
});
