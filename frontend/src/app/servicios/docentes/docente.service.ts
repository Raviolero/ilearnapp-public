import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Docente } from '../../modelos/docente';
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class DocenteService {

  selectedDocente: Docente;
  docentes: Docente[];
  readonly URL_API = 'http://localhost:3000/api/docentes';

  constructor(private http: HttpClient) {
    this.selectedDocente = new Docente();
  }
  getDocentes() {
    return this.http.get(this.URL_API);
  }
  getDocenteEmail(_id: string) {
    return this.http.get(this.URL_API + `/${_id}`);
  }
  postDocente(docente: Docente) {
    return this.http.post(this.URL_API, docente);
  }
  putDocente(docente: Docente) {
    return this.http.put(this.URL_API + `/${docente._id}`, docente);
  }
  getDocente(id: string) {
    return this.http.get(this.URL_API + `/${id}`);
  }
  // tslint:disable-next-line:variable-name
  deleteDocente(_id: string) {
    return this.http.delete(this.URL_API + `/${_id}`);
  }
}



