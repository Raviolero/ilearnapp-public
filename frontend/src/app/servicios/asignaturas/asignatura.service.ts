import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Asignatura } from '../../modelos/asignatura';

@Injectable({
  providedIn: 'root'
})
export class AsignaturaService {

  selectedAsignatura: Asignatura;
  asignaturas: Asignatura[];
  readonly URL_API = 'http://localhost:3000/api/asignaturas';

  constructor(private http: HttpClient) {
    this.selectedAsignatura = new Asignatura();
  }
  getAsignaturas() {
    return this.http.get(this.URL_API);
  }
  postAsignatura(asignatura: Asignatura) {
    return this.http.post(this.URL_API, asignatura);
  }
  putAsignatura(asignatura: Asignatura) {
    return this.http.put(this.URL_API + `/${asignatura._id}`, asignatura);
  }
  getAsignatura(id: string) {
    return this.http.get(this.URL_API + `/${id}`);
  }
  // tslint:disable-next-line:variable-name
  deleteAsignatura(_id: string) {
    return this.http.delete(this.URL_API + `/${_id}`);
  }
  getUnaAsignatura(_id: string) {
    return this.http.get(this.URL_API + `/${_id}`);
  }
}
