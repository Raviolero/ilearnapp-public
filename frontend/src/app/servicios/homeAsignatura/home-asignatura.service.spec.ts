import { TestBed } from '@angular/core/testing';

import { HomeAsignaturaService } from './home-asignatura.service';

describe('HomeAsignaturaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HomeAsignaturaService = TestBed.get(HomeAsignaturaService);
    expect(service).toBeTruthy();
  });
});
