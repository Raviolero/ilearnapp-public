import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Alumno } from '../../modelos/alumno';

@Injectable({
  providedIn: 'root'
})
export class AlumnoService {

  selectedAlumno: Alumno;
  alumnos: Alumno[];
  readonly URL_API = 'http://localhost:3000/api/alumnos';

  constructor(private http: HttpClient) {
    this.selectedAlumno = new Alumno();
  }
  getAlumnos() {
    return this.http.get(this.URL_API);
  }
  postAlumno(alumno: Alumno) {
    return this.http.post(this.URL_API, alumno);
  }
  putAlumno(alumno: Alumno) {
    return this.http.put(this.URL_API + `/${alumno._id}`, alumno);
  }
  // tslint:disable-next-line:variable-name
  deleteAlumno(_id: string) {
    return this.http.delete(this.URL_API + `/${_id}`);
  }
}
