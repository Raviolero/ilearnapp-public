import { Component, OnInit } from '@angular/core';
import { AdminService } from '../admin.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
declare var $:any;
declare var M:any;
@Component({
  selector: 'app-adminhome',
  templateUrl: './adminhome.component.html',
  styleUrls: ['./adminhome.component.css']
})
export class AdminhomeComponent implements OnInit {
  show: boolean = true;
  ABMdocente: boolean = false;
  ABMalumno: boolean = false;
  ABMasignatura: boolean = false;
  vincula: boolean = false;

  nombre: string;
  email: string;
  constructor(private T: ToastrService, private _admin: AdminService, private _router: Router) {
    this.nombre = 'julio';

    this._admin.adminHome()
      .subscribe(
        data => {
          this.email = data['email'];
        },
        error => {
          this.T.error(error['statusText']);
          this._router.navigate(['/login']);
        }
      )
  }

  ngOnInit() {
    $(document).ready(function(){
      $('.sidenav').sidenav(
        {
          edge: 'left', 
          closeOnClick: false,
          draggable: true,
          preventScrolling: true,
        }
      );
    });      
  }

  logout() {
    this._admin.logout()
      .subscribe(
        data => {
          this.T.info(data['message']);
          this._router.navigate(['/login']);
        },
        error => {
          //console.error(error);
          this.T.error(error['statusText']);
        }
      )
  }
  mostrarDoc() {
    this.ABMdocente = true;
    this.ABMalumno = false;
    this.ABMasignatura = false;
    this.vincula = false;
  }
  mostrarAlu() {
    this.ABMdocente = false;
    this.ABMalumno = true;
    this.ABMasignatura = false;
    this.vincula = false;
  }
  mostrarAsig() {
    this.ABMdocente = false;
    this.ABMalumno = false;
    this.ABMasignatura = true;
    this.vincula = false;
  }
  funcionVincular() {
    this.ABMdocente = false;
    this.ABMalumno = false;
    this.ABMasignatura = false;
    this.vincula = true;
  }
  
  esconderDoc() {
    this.ABMdocente = false;
  }



}
