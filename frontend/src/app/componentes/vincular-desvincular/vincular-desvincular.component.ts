import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { FormControl } from "@angular/forms";
import { Observable } from "rxjs";
import { map, startWith } from "rxjs/operators";
import { DocenteService } from "src/app/servicios/docentes/docente.service";
import { Docente } from "src/app/modelos/docente";
import { Asignatura } from "src/app/modelos/asignatura";
import { AsignaturaService } from "src/app/servicios/asignaturas/asignatura.service";

//--------------Declaracion de variables--------------
declare var $: any; //Esta variable sirve para que funcione JQuery
declare var M: any; //Esta variable sirve para que funcione el Materialize

@Component({
  selector: "app-vincular-desvincular",
  templateUrl: "./vincular-desvincular.component.html",
  styleUrls: ["./vincular-desvincular.component.css"]
})
export class VincularDesvincularComponent implements OnInit {
  constructor(
    private docenteServicio: DocenteService,
    private asignaturaServicio: AsignaturaService
  ) {}

  //--------------Declaracion de variables locales--------------
  myControl = new FormControl(); //Sirve para autocompletado
  opciones: Docente[]; //Guarda todos los docentes como opciones a ser desplegados en el autocompletado
  filtradoOpciones: Observable<Docente[]>; //Es una variable que guarda los docentes filtrados

  async ngOnInit() {
    //Obtengo todas las asignaturas
    this.getAsignaturas();

    //Obtengo todos los docentes
    this.getDocentes();

    //Pido un time delay porque no logro hacer que lo espere.
    await this.delay(1000);

    //Cargo todos los docentes obtenidos como opciones posibles
    this.opciones = this.docenteServicio.docentes;

    //Esto es una funcion que encontre por internet. Prestar mucha atencion...
    this.filtradoOpciones = this.myControl.valueChanges.pipe(
      startWith(""),
      map(valor => (typeof valor === "string" ? valor : valor.nombre)),
      map(nombre => (nombre ? this._filter(nombre) : this.opciones.slice()))
    );

  }

  /**
   * Retarda la ejecucion por una X cantidad de tiempo expresados en milisegundos.
   * @param ms Numero que expresa la cantidad de tiempo en milisegundos que hara el delay.
   */
  delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  /**
   * Obtiene todas las asignaturas cargadas en el sistema.
   */
  getAsignaturas() {
    this.asignaturaServicio.getAsignaturas().subscribe(res => {
      this.asignaturaServicio.asignaturas = res as Asignatura[];
    });
  }

  /**
   * Obtiene todos los docentes cargados en el sistema.
   */
  getDocentes() {
    this.docenteServicio.getDocentes().subscribe(res => {
      this.docenteServicio.docentes = res as Docente[];
    });
  }

  /**
   * No estoy seguro bien que hace esta funcion. Sorry.
   * @param docente 
   */
  displayFn(docente?: Docente): string | undefined {
    return docente ? docente.nombre + " " + docente.apellido : undefined;
  }

  /**
   * Filtra las opciones que posee de acuerdo a un nombre proporcionado.
   * @param nombre Obtiene un nombre que servira para filtrar la busqueda entre sus opciones.
   */
  private _filter(nombre: string): Docente[] {
    //Toma el nombre proporcionado y lo convierte en miniscula
    const filterValue = nombre.toLowerCase();

    //Retorno las opciones filtradas
    return this.opciones.filter(
      opciones => opciones.nombre.toLowerCase().indexOf(filterValue) === 0
    );
  }
}
