import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VincularDesvincularComponent } from './vincular-desvincular.component';

describe('VincularDesvincularComponent', () => {
  let component: VincularDesvincularComponent;
  let fixture: ComponentFixture<VincularDesvincularComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VincularDesvincularComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VincularDesvincularComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
