import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadArchivoComponent } from './upload-archivo.component';

describe('UploadArchivoComponent', () => {
  let component: UploadArchivoComponent;
  let fixture: ComponentFixture<UploadArchivoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadArchivoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadArchivoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
