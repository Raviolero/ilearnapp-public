import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocentehomeComponent } from './docentehome.component';

describe('DocentehomeComponent', () => {
  let component: DocentehomeComponent;
  let fixture: ComponentFixture<DocentehomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocentehomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocentehomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
