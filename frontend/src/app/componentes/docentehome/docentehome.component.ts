import { Component, OnInit } from "@angular/core";
import { AdminService } from "../../admin.service";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { DocenteService } from "src/app/servicios/docentes/docente.service";

@Component({
  selector: "app-docentehome",
  templateUrl: "./docentehome.component.html",
  styleUrls: ["./docentehome.component.css"]
})
export class DocentehomeComponent implements OnInit {
  show: boolean = true;
  datosDocente: any;
  listaAsignaturas: any;
  idDocente: any;

  constructor(
    private _docent: DocenteService,
    private T: ToastrService,
    private _admin: AdminService,
    private _router: Router
  ) {
    this._admin.adminHome().subscribe(
      data => {
        this.cargarAsignaturas(data["_id"]); // data["_id"] contiene el id del docente
        this.show = true;
      },
      error => {
        this.T.error(error["statusText"]);
        this._router.navigate(["/login"]);
      }
    );

  }

  cargarAsignaturas(id: string) {
    // este metodo obtiene el id del docente para buscar sus asignaturas
    var materias: any[]; // se declara un vector que contendra las materias obtenidas desde el servidor
    this._docent.getDocenteEmail(id).subscribe(
      // se pide al servidor el docente, dicho ob dentro contiene sus asignaturas
      data => {
        materias = data["asignatura"]; // se asigna a materias el sub conjundo de datos (array de materias) contenidas en el objeto docente
        for (let index = 0; index < materias.length; index++) {
          materias[index] = materias[index]["nombre"]; // por cada objeto materia se extrae y almacena solamente su nombre
        }
        this.listaAsignaturas = materias; // al finalizar la extraccion se asigna el resultado de todo el procedimiento a la variable de clase llamda listaAsignaturas
        // la misma se encuentra vinculada a las filas de una tabla en la vista que se cargan mediante un *ngFor
      }
    );
  }

  ngOnInit() {}

  irAsignatura(){
    this._router.navigate(['/asignaturaHome']);
  }

  logout() {
    this._admin.logout().subscribe(
      data => {
        console.log(data);
        this.T.info(data["message"]);
        this._router.navigate(["/login"]);
      },
      error => {
        console.error(error);
        this.T.error(error["statusText"]);
      }
    );
  }

}
