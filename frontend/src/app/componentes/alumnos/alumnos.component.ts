import { Component, OnInit } from '@angular/core';
import { AlumnoService } from '../../servicios/alumnos/alumno.service';
import { NgForm } from '@angular/forms';
import { Alumno } from 'src/app/modelos/alumno';

declare var M: any;

@Component({
  selector: 'app-alumnos',
  templateUrl: './alumnos.component.html',
  styleUrls: ['./alumnos.component.css']
})
export class AlumnosComponent implements OnInit {

  constructor(private alumnoServicio: AlumnoService) { }

  ngOnInit() {
    this.getAlumnos();
  }

  resetFormulario(form?: NgForm) {
    if (form) {
      form.reset();
      this.alumnoServicio.selectedAlumno = new Alumno();
    }
  }

  addAlumno(form: NgForm) {
    if (form.value._id) {
      this.alumnoServicio.putAlumno(form.value)
        .subscribe(res => {
          this.resetFormulario(form);
          M.toast({html: 'Alumno actualizado'});
          this.getAlumnos();
        });
    } else {
      this.alumnoServicio.postAlumno(form.value)
      .subscribe( res => {
        this.resetFormulario(form);
        M.toast({html: 'Alumno guardado'});
        this.getAlumnos();
      });
    }
  }

  getAlumnos() {
    this.alumnoServicio.getAlumnos()
      .subscribe(res => {
        this.alumnoServicio.alumnos = res as Alumno[];
        console.log(res);
      });
  }

  editAlumno(alumno: Alumno) {
    this.alumnoServicio.selectedAlumno = alumno;
  }

  deleteAlumno(alumno: Alumno) {
    if (confirm('Estas seguro de eliminar?')) {
      this.alumnoServicio.deleteAlumno(alumno._id)
      .subscribe(res => {
        this.getAlumnos();
        M.toast({html: 'Eliminado exitoso'});
      });
    }
  }

}
