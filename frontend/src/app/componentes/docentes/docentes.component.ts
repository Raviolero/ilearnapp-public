import { Component, OnInit } from '@angular/core';
import { DocenteService } from '../../servicios/docentes/docente.service';
import { NgForm } from '@angular/forms';
import { Docente } from 'src/app/modelos/docente';


declare var M: any;

@Component({
  selector: 'app-docentes',
  templateUrl: './docentes.component.html',
  styleUrls: ['./docentes.component.css']
})
export class DocentesComponent implements OnInit {

  constructor(private docenteServicio: DocenteService) { }

  ngOnInit() {
    this.getDocentes();
  }

  //limpia el formulario de ingresar datos del docente
  resetFormulario(form?: NgForm) {
    if (form) {
      form.reset();
      this.docenteServicio.selectedDocente = new Docente();
    }
  }

  addDocente(form: NgForm) {
    if (form.value._id) {
      this.docenteServicio.putDocente(form.value)
        .subscribe(res => {
          this.resetFormulario(form);
          M.toast({html: 'Docente actualizado'});
          this.getDocentes();
        });
    } else {
      this.docenteServicio.postDocente(form.value)
      .subscribe( res => {
        this.resetFormulario(form);
        M.toast({html: 'Docente guardado'});
        this.getDocentes();
      });
    }
  }

  getDocentes() {
    this.docenteServicio.getDocentes()
      .subscribe(res => {
        this.docenteServicio.docentes = res as Docente[];
        console.log(res);
      });
  }

  editDocente(docente: Docente) {
    this.docenteServicio.selectedDocente = docente;
  }

  deleteDocente(docente: Docente) {
    if (confirm('Estas seguro de eliminar?')) {
      this.docenteServicio.deleteDocente(docente._id)
      .subscribe(res => {
        this.getDocentes();
        M.toast({html: 'Eliminado exitoso'});
      });
    }
  }

}
