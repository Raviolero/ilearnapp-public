import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeAsignaturaComponent } from './home-asignatura.component';

describe('HomeAsignaturaComponent', () => {
  let component: HomeAsignaturaComponent;
  let fixture: ComponentFixture<HomeAsignaturaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeAsignaturaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeAsignaturaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
