import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ActivatedRoute, Params } from "@angular/router";
import { DocenteService } from "src/app/servicios/docentes/docente.service";
import { AsignaturaService } from "src/app/servicios/asignaturas/asignatura.service";
import { HttpClient } from "@angular/common/http";
import { ToastrService } from "ngx-toastr";
declare var $: any;
@Component({
  selector: "app-home-asignatura",
  templateUrl: "./home-asignatura.component.html",
  styleUrls: ["./home-asignatura.component.css"]
})
export class HomeAsignaturaComponent implements OnInit {
  uploadedFiles: Array<File>=null;
  show: boolean = false;
  //datos docente
  IdDocente: string;
  id: string;
  email: string;
  nombre: string;
  apellido: string;
  //datos asignatura
  idAsignatura: string;
  idAsign: string;
  nombrAsign: string;
  anioAsign: string;
  descripAsign: string;
  pdfsAsign: string[];

  constructor(
    private http: HttpClient,
    private _router: Router,
    private rutaActiva: ActivatedRoute,
    private _docente: DocenteService,
    private _asign: AsignaturaService,
    private T: ToastrService
  ) {
    $(document).ready(function() {
      $(".modal").modal();
    });
    $(document).ready(function() {
      $(".sidenav").sidenav({
        edge: "left"
      });
    });
    $(document).ready(function() {
      $("#slide-out2").sidenav({
        edge: "right"
      });
    });
  }
  fileChange(element) {
    this.uploadedFiles = element.target.files;
  }
  upload() {
    let formData = new FormData();
    if (this.uploadedFiles != null) {
      for (var i = 0; i < this.uploadedFiles.length; i++) {
        formData.append(
          "pdf",
          this.uploadedFiles[i],
          this.uploadedFiles[i].name
          
        );
      }
      console.log('el nombre del fichero es: ', this.uploadedFiles[0].name);
      this.http
        .put(
          "http://localhost:3000/api/asignaturas/archivo/" + this.idAsignatura,
          formData
        )
        .subscribe(response => {
          var resultado = response["asignatura"];
          if (resultado["ok"] === 1) {
            this.T.success("Carga exitosa");
            this.uploadedFiles = null;
          } else {
            this.T.error("Error al subir archivo");
          }
          console.log("la respuesta del servidor es:  ", response);
          this.actualizarDatosAsignatura();
          // this._router.navigate(['/asignaturaHome/'+this.IdDocente+'/'+this.idAsignatura]);
        });
    }else{
      this.T.error('Ningun archivo seleccionado');
    }
  }
  ngOnInit() {
    this.IdDocente = this.rutaActiva.snapshot.params.idDocente;
    this.idAsignatura = this.rutaActiva.snapshot.params.idAsignatura;
    this.actualizarDatosAsignatura();
    this.actualizarDatosDocente();
  }

  actualizarDatosAsignatura() {
    this._asign.getAsignatura(this.idAsignatura).subscribe(data => {
      this.idAsign = data["id"];
      this.nombrAsign = data["nombre"];
      this.anioAsign = data["anio"];
      this.descripAsign = data["descripcion"];
      this.pdfsAsign = data["pdfUrl"];
    });
  }
  actualizarDatosDocente() {
    this._docente.getDocente(this.IdDocente).subscribe(data => {
      this.id = data["id"];
      this.email = data["email"];
      this.nombre = data["nombre"];
      this.apellido = data["apellido"];
    });
  }

  mostrarEsconder() {
    if (this.show) {
      this.show = false;
    } else {
      this.show = true;
    }
  }

  irVincularDesvincular() {
    this._router.navigate(["/vincular-desvincular"]);
  }
}
