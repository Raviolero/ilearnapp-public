import { Component, OnInit } from '@angular/core';
import { AsignaturaService } from '../../servicios/asignaturas/asignatura.service';
import { NgForm } from '@angular/forms';
import { Asignatura } from 'src/app/modelos/asignatura';

declare var M: any;

@Component({
  selector: 'app-asignaturas',
  templateUrl: './asignaturas.component.html',
  styleUrls: ['./asignaturas.component.css']
})
export class AsignaturasComponent implements OnInit {

  constructor(private asignaturaServicio: AsignaturaService) { }

  ngOnInit() {
    this.getAsignaturas();
  }

  resetFormulario(form?: NgForm) {
    if (form) {
      form.reset();
      this.asignaturaServicio.selectedAsignatura = new Asignatura();
    }
  }

  addAsignatura(form: NgForm) {
    if (form.value._id) {
      this.asignaturaServicio.putAsignatura(form.value)
        .subscribe(res => {
          this.resetFormulario(form);
          M.toast({html: 'Asignatura actualizada'});
          this.getAsignaturas();
        });
    } else {
      this.asignaturaServicio.postAsignatura(form.value)
      .subscribe( res => {
        this.resetFormulario(form);
        M.toast({html: 'Asignatura guardada'});
        this.getAsignaturas();
      });
    }
  }

  getAsignaturas() {
    this.asignaturaServicio.getAsignaturas()
      .subscribe(res => {
        this.asignaturaServicio.asignaturas = res as Asignatura[];
        console.log(res);
      });
  }

  editAsignatura(asignatura: Asignatura) {
    this.asignaturaServicio.selectedAsignatura = asignatura;
  }

  deleteAsignatura(asignatura: Asignatura) {
    if (confirm('Estas seguro de eliminar?')) {
      this.asignaturaServicio.deleteAsignatura(asignatura._id)
      .subscribe(res => {
        this.getAsignaturas();
        M.toast({html: 'Eliminado exitoso'});
      });
    }
  }

}
