import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AdminService } from '../admin.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  
  loginForm:FormGroup = new FormGroup({ // se crea un objeto formulario para vincular con el formulario de login
    email:new FormControl(null,[Validators.email,Validators.required]), // se crean los campos con sus respectivas validaciones
    password:new FormControl(null,Validators.required)
  });

  constructor(private T:ToastrService,private _router:Router,private _admin:AdminService) { }

  ngOnInit() {
  }

  login(){ // funcion enlazada al boton login del formulario de login
    if(!this.loginForm.valid){
      this.T.error('Formato de entrada inválido');
      console.log('login invalido'); // si el form no es valido se imprime error y se sale
      return;
    }
    
    this._admin.login(JSON.stringify(this.loginForm.value))
    .subscribe(
      data => {
        //console.log(data);
        this.T.success(data['message']);
        if(data['tipo']==="admin"){
          this._router.navigate(['/adminHome']);
        }else{
          this._router.navigate(['/docenteHome']);
        }
        
      },
      error => {
      //console.error(error);
      this.T.error(error.error['message']);
      this._router.navigate(['/login']);
      }
    )
  }
}
