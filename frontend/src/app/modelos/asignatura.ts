export class Asignatura {
    constructor(_id = '', id ='', nombre = '', anio = '', descripcion = ''){
        this._id = _id;
        this.id = id;
        this.nombre = nombre;
        this.anio = anio;
        this.descripcion = descripcion;
    }

    // tslint:disable-next-line:variable-name
    _id: string;
    id: string;
    nombre: string;
    anio: string;
    descripcion: string;
}
