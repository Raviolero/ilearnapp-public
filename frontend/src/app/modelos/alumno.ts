export class Alumno {
    constructor(_id = '', dni = 0, nombre = '', edad = 0, direccion = ''){
        this._id = _id;
        this.dni = dni;
        this.nombre = nombre;
        this.edad = edad;
        this.direccion = direccion;
    }

    // tslint:disable-next-line:variable-name
    _id: string;
    dni: number;
    nombre: string;
    edad: number;
    direccion: string;
}
