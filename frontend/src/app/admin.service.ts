import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(private _http:HttpClient) { } 

  login(body:any){
    return this._http.post('http://127.0.0.1:3000/api/login',body,{
      observe:'body',
      withCredentials:true,// estos tres parametros son necesarios para que pasemos al servidor las cookies del navegador y se puedan administrar las sesiones
      headers:new HttpHeaders().append('Content-Type','application/json')
      });
  }

   adminHome(){
    return  this._http.get('http://127.0.0.1:3000/api/login/validar',{
      observe:'body',
      withCredentials:true, 
      headers:new HttpHeaders().append('Content-Type','application/json')
      })
    
  } 

  logout(){
    return this._http.get('http://127.0.0.1:3000/api/login/logout',{
      observe:'body',
      withCredentials:true, 
      headers:new HttpHeaders().append('Content-Type','application/json')
      });
  }
}

