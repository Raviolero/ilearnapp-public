import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { AdminService } from '../admin.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class IslogGuard implements CanActivate {
  constructor(private _admin: AdminService, private T: ToastrService,private _router:Router) {


  }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean>  {
  
    return this._admin.adminHome().pipe(
      map(e => {
        if (e) {
          return true;
        }else {
          this._router.navigate(['/login']);
          return false;
        }
      }),
    );
  }

}
