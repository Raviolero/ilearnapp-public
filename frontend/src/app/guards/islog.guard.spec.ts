import { TestBed, async, inject } from '@angular/core/testing';

import { IslogGuard } from './islog.guard';

describe('IslogGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IslogGuard]
    });
  });

  it('should ...', inject([IslogGuard], (guard: IslogGuard) => {
    expect(guard).toBeTruthy();
  }));
});
