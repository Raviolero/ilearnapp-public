import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AdminService } from '../admin.service';
import { map } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class IsadminGuard implements CanActivate {
  constructor(private _admin: AdminService,private T: ToastrService,private _router:Router){}
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean>  {
    return this._admin.adminHome().pipe(
      map(e => {
        if (e['tipo']=='admin') {
          return true;
        } else {
          this.T.error('No Autorizado');
          this._router.navigate(['/docentehome']);
        }
      }),
    );
  }
  
}
