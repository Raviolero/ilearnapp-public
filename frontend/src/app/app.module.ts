import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AlumnosComponent } from './componentes/alumnos/alumnos.component';
import { LoginComponent } from './login/login.component';
import { AdminhomeComponent } from './adminhome/adminhome.component';
import {ReactiveFormsModule,FormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {AdminService} from './admin.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { VincularDesvincularComponent } from './componentes/vincular-desvincular/vincular-desvincular.component';
import { AsignaturasComponent } from './componentes/asignaturas/asignaturas.component';
import { DocentesComponent } from './componentes/docentes/docentes.component';
import {
  MatAutocompleteModule,
  MatInputModule
} from '@angular/material';
import { DocentehomeComponent } from './componentes/docentehome/docentehome.component';
import { IslogGuard } from './guards/islog.guard';

import { AltaAlumnoComponent } from './componentes/alta-alumno/alta-alumno.component';

import { UploadArchivoComponent } from './componentes/upload-archivo/upload-archivo.component';
import { HomeAsignaturaComponent } from './componentes/home-asignatura/home-asignatura.component';


@NgModule({
  declarations: [
    AppComponent,
    AlumnosComponent,
    LoginComponent,
    AdminhomeComponent,
    VincularDesvincularComponent,
    AsignaturasComponent,
    DocentesComponent,
    DocentehomeComponent,

    AltaAlumnoComponent,

    UploadArchivoComponent,
    HomeAsignaturaComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      timeOut:2000,
      positionClass:'toast-top-right',
      preventDuplicates:false
    }),
    MatAutocompleteModule,
    MatInputModule
  ],
  providers: [
    AdminService,
    IslogGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
